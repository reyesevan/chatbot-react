import React from "react";
import ReactDOM from "react-dom";
import "./style.css";

import Graph from "./graph/Graph";
import GraphVertex from "./graph/GraphVertex";
import GraphEdge from "./graph/GraphEdge";


var graph = new Graph(true);

const vertices = [
	new GraphVertex("bot", "Bienvenido"), //0
	new GraphVertex("bot", "Mi nombre es B-Bot"), //1
	new GraphVertex("bot", "¿Te puedo ayudar?"), //2
	new GraphVertex("option", "Sí, por favor"), //3
	new GraphVertex("option", "No, gracias"), //4
	new GraphVertex("bot", "¿En qué servicio estás interesado?"), //5
	new GraphVertex("option", "Desarrollo web y Hosting"), //6
	new GraphVertex("option", "Producción visual"), //7
	new GraphVertex("option", "Social Media"), //8
	new GraphVertex("option", "Branding"), //9
	new GraphVertex("bot", "Claro, en Desarrollo Web y Hosting hacemos lo siguiente: "), //10
	new GraphVertex("bot", "Claro, en Producción Visual hacemos lo siguiente: "), //11
	new GraphVertex("bot", "Claro, en Social Media hacemos lo siguiente: "), //12
	new GraphVertex("bot", "Claro, en Branding hacemos lo siguiente: "), //13
	new GraphVertex("bot", "· Páginas Web"), //14 Web & Hosting services
	new GraphVertex("bot", "· Análisis SEO"), //15
	new GraphVertex("bot", "· Web Services"), //16
	new GraphVertex("bot", "· Portal informativo / noticias"), //17
	new GraphVertex("bot", "· E-commerce"), //18
	new GraphVertex("bot", "· Hosting"), //19
	new GraphVertex("bot", "· Blog"), //20
	new GraphVertex("bot", "· Jingles"), //21 Visual Production services
	new GraphVertex("bot", "· Himno institucional"), //22
	new GraphVertex("bot", "· Video corporativo"), //23
	new GraphVertex("bot", "· Timelapse"), //24
	new GraphVertex("bot", "· Documentales"), //25
	new GraphVertex("bot", "· Entrevistas"), //26
	new GraphVertex("bot", "· Videos 360°"), //27
	new GraphVertex("bot", "· Imágenes 360°"), //28
	new GraphVertex("bot", "· Spots de radio"), //29
	new GraphVertex("bot", "· Anuncios de TV"), //30
	new GraphVertex("bot", "· Brief-Mind"), //31 Social media services
	new GraphVertex("bot", "· Conceptualización creativa"), //32
	new GraphVertex("bot", "· Creatividad + diseño multimedia"), //33
	new GraphVertex("bot", "· Shooting fotográfico + Banco de imágenes (formato de redes sociales)"), //34
	new GraphVertex("bot", "· Optimización de perfiles"), //35
	new GraphVertex("bot", "· Monitoreo de redes"), //36
	new GraphVertex("bot", "· Account manager personalizado"), //37
	new GraphVertex("bot", "· Manual de preguntas frecuentes"), //38
	new GraphVertex("bot", "· Manual de flujos operativos"), //39
	new GraphVertex("bot", "· Directorio corporativo"), //40
	new GraphVertex("bot", "· Crisis management"), //41
	new GraphVertex("bot", "· Estrategia de campañas de anuncios"), //42
	new GraphVertex("bot", "· Creación o actualización de logo"), //43 Branding services
	new GraphVertex("bot", "· Naming"), //44
	new GraphVertex("bot", "· Manual de identidad"), //45
	new GraphVertex("bot", "· Identidad corporativa"), //46
	new GraphVertex("bot", "· Branding a la medida"), //47
	new GraphVertex("bot", "· Presentaciones corporativas y de ventas"), //48
	new GraphVertex("bot", "· Infografías"), //49
	new GraphVertex("bot", "· Motion graphics (animación de logos, firmas electrónicas, cinemagrafías)"), //50
	new GraphVertex("bot", "¿Te gustaría conocer otro servicio?"), //51
	new GraphVertex("bot", "De acuerdo."), //52
	new GraphVertex("bot", "¡Me dio gusto platicar contigo!"), //53
	new GraphVertex("bot", "Ten un excelente día."), //54
	new GraphVertex("link", "¡CONTÁCTANOS AQUÍ!"), //55
];

const edges = [
	new GraphEdge(vertices[0], vertices[1]),
	new GraphEdge(vertices[1], vertices[2]),
	new GraphEdge(vertices[2], vertices[3]),
	new GraphEdge(vertices[2], vertices[4]),
	new GraphEdge(vertices[3], vertices[5]),
	new GraphEdge(vertices[5], vertices[6]),
	new GraphEdge(vertices[5], vertices[7]),
	new GraphEdge(vertices[5], vertices[8]),
	new GraphEdge(vertices[5], vertices[9]),
	new GraphEdge(vertices[6], vertices[10]),
	new GraphEdge(vertices[7], vertices[11]),
	new GraphEdge(vertices[8], vertices[12]),
	new GraphEdge(vertices[9], vertices[13]),
	new GraphEdge(vertices[10], vertices[14]),
	new GraphEdge(vertices[10], vertices[15]),
	new GraphEdge(vertices[10], vertices[16]),
	new GraphEdge(vertices[10], vertices[17]),
	new GraphEdge(vertices[10], vertices[18]),
	new GraphEdge(vertices[10], vertices[19]),
	new GraphEdge(vertices[10], vertices[20]),
	new GraphEdge(vertices[10], vertices[55]),
	new GraphEdge(vertices[11], vertices[21]),
	new GraphEdge(vertices[11], vertices[22]),
	new GraphEdge(vertices[11], vertices[23]),
	new GraphEdge(vertices[11], vertices[24]),
	new GraphEdge(vertices[11], vertices[25]),
	new GraphEdge(vertices[11], vertices[26]),
	new GraphEdge(vertices[11], vertices[27]),
	new GraphEdge(vertices[11], vertices[28]),
	new GraphEdge(vertices[11], vertices[29]),
	new GraphEdge(vertices[11], vertices[30]),
	new GraphEdge(vertices[11], vertices[55]),
	new GraphEdge(vertices[12], vertices[31]),
	new GraphEdge(vertices[12], vertices[32]),
	new GraphEdge(vertices[12], vertices[33]),
	new GraphEdge(vertices[12], vertices[34]),
	new GraphEdge(vertices[12], vertices[35]),
	new GraphEdge(vertices[12], vertices[36]),
	new GraphEdge(vertices[12], vertices[37]),
	new GraphEdge(vertices[12], vertices[38]),
	new GraphEdge(vertices[12], vertices[39]),
	new GraphEdge(vertices[12], vertices[40]),
	new GraphEdge(vertices[12], vertices[41]),
	new GraphEdge(vertices[12], vertices[42]),
	new GraphEdge(vertices[12], vertices[55]),
	new GraphEdge(vertices[13], vertices[43]),
	new GraphEdge(vertices[13], vertices[44]),
	new GraphEdge(vertices[13], vertices[45]),
	new GraphEdge(vertices[13], vertices[46]),
	new GraphEdge(vertices[13], vertices[47]),
	new GraphEdge(vertices[13], vertices[48]),
	new GraphEdge(vertices[13], vertices[49]),
	new GraphEdge(vertices[13], vertices[50]),
	new GraphEdge(vertices[13], vertices[55]),
	new GraphEdge(vertices[20], vertices[51]),
	new GraphEdge(vertices[30], vertices[51]),
	new GraphEdge(vertices[42], vertices[51]),
	new GraphEdge(vertices[10], vertices[51]),
	new GraphEdge(vertices[10], vertices[3]),
	new GraphEdge(vertices[10], vertices[4]),
	new GraphEdge(vertices[11], vertices[51]),
	new GraphEdge(vertices[11], vertices[3]),
	new GraphEdge(vertices[11], vertices[4]),
	new GraphEdge(vertices[12], vertices[51]),
	new GraphEdge(vertices[12], vertices[3]),
	new GraphEdge(vertices[12], vertices[4]),
	new GraphEdge(vertices[13], vertices[51]),
	new GraphEdge(vertices[13], vertices[3]),
	new GraphEdge(vertices[13], vertices[4]),
	new GraphEdge(vertices[4], vertices[52]),
	new GraphEdge(vertices[52], vertices[53]),
	new GraphEdge(vertices[53], vertices[54]),
];

vertices.forEach(vertex => graph.addVertex(vertex));
edges.forEach(edge => graph.addEdge(edge));

const exit = new GraphVertex("option", "Haga clic aquí para salir"); 

const botavatar = "http://www.redmind.mx/demo/berna-icon.png";
const useravatar = "https://cdn3.iconfinder.com/data/icons/business-avatar-1/512/7_avatar-512.png";

const link = "http://www.redmind.mx/contacto.html";
const linkIcon = "http://icon-park.com/imagefiles/link_icon_red.png";

const hideIcon = "https://image.flaticon.com/icons/png/512/106/106902.png";
const showIcon = "https://cdn3.iconfinder.com/data/icons/office-general-4/64/eye-512.png";

const helpMessage = "¿Te puedo ayudar?"

/**
 * Returns a fraction of the graph corresponding to the responses of the bot, the question and the user options (if any)
 * from the specified vertex
 * @param {*} vertex
 */
function sliceGraph(graph, vertexKey) {
	var vertex = graph.getVertexByKey(vertexKey);
	var currentDialog = [vertex];
	var neighbors;
	do {
		neighbors = vertex.getNeighbors();

		neighbors.forEach(vertex => currentDialog.push(vertex));
		vertex = neighbors[0];
	} while (neighbors.length === 1);
	return currentDialog;
}

/**
 * Auxiliar function and variables to automatically generate unique keys 
 */
var map = new WeakMap();
var index = 0;
function weakKey(obj) {
	var key = map.get(obj);
	if (!key) {
		key = "weak-key-" + index++;
		map.set(obj, key);  
	}
	return key;
}

/**
 * Custom component, represents the animated widget located at the bottom-right corner
 * of the screen. 
 * 
 * It consists of the following components:
 *  <#startWidget> --> the red rounded rectangle surrounding the icon. Gets wider and white when hover
 * 		<#avatarIcon/> --> avatar's bot
 * 		<#helpMessage/> --> "¿Te puedo ayudar?" message. Only visible when hover
 * 		<#startChat/> --> the red button to start the chat. Only visible when hover
 *  </#startWidget>
 */
class StartButton extends React.Component {
	constructor(props) {
		super(props);
		
		this.state = {
			chatStarted: false,
		}
	}

	render() {
		return this.state.chatStarted ? 
			<Chat /> : 
			<div id = "startWidget">
				<img
					id = "avatarIcon"
					src = {botavatar}
					alt = ""
				/>
				<p id = "helpMessage">{helpMessage}</p>
				<button
					id = "startChat"
					onClick = {() => this.setState({ chatStarted: true })}
				>
					Iniciar chat
				</button>
			</div>
	}
}

/**
 * Represents the messages "sent" by the bot and the user.
 * They are constructed based on the data stored in every vertex of the graph if the type is either "bot" or "user":
 * GraphVertex.type === chatbubble.sender
 * GraphVertex.value === chatbubble.message
 * 
 * It consists of the following elements:
 *  <.chatbubble> --> the rounded box that surrounds the message. Switching colors and positions depending on the sender
 * 		<.avatar> --> the avatar of the corner of every message. Changes image dependeing on the sender 
 * 		message
 *  </.chatbubble> -->
 * @param {*} props 
 */
function ChatBubble(props) {
	const url = props.sender === "bot" ? botavatar : useravatar;

	return (
		<div
		className = "chatbubble"
		sender = {props.sender}
		key = {weakKey(props.k)}
		>
			<img
				className = "avatar"
				src = {url}
				width = "30px"
				height = "30px"
				sender = {props.sender}
				alt = ""
				/>
			{props.children}
		</div>
	);
}

/**
 * Represents the clickeable options that the user can choose from.
 * They are constructed based on the data stored in every vertex of the graph whose type is "option"
 * GraphVertex.type === optionbubble.sender
 * GraphVertex.value === optionbubble.message
 * 
 * It consists of the following elements:
 *  <.option>
 *  	message
 *  </.option>
 */
function OptionBubble(props) {
	return (
		<div
			className="option"
			key={weakKey(props.k)}
			onClick={() => props.onClick(props.k)}
		>
			{props.children}
		</div>
	);
}

/**
 * Represents the clickeable links the user can follow.
 * They are constructed based on the data stored in every vertex of the graph whose type is "link"
 * 
 * It consists of the following elements:
 *  <.link> --> the clickeable rounded box that surrounds the message and icons. Its background changes on hover. Its reference can be changed with the link variable
 *  	<.linkIcon>  --> the icon at the left of the box that indicates that it represents a link. Its image can be changed with the linkIcon variable.
 *  	message
 *  </.link>
 */ 	
function LinkBubble(props) {
	return (
		<a
			className="link"
			key={weakKey(props.k)}
			href = {link}
		>
			<img
				className = "linkIcon"
				src = {linkIcon}
				width = "40px"
				height = "40px"
				alt = ""
			/>
			{props.children}
		</a>
	);
}

/**
 * Component rendered when the startchat button is clicked,
 * it's container of all the bubbles and manages the logic for user interaction.
 * It uses the graph to know what dialogs to render at anytime.
 * When rendered, it may be hidden by cicking outside the chat window or the togglevisibility button
 * 
 * It consists of the following elements:
 *  <.togglevisibility> --> the red tab to toggles the status of the chat window between shown and hidden 
 *  <#chat> --> the translucent box that contains all the bubbles
 *  	{.chatbubbles} --> the set of bubbles sent by the bot and the user
 * 		[.linkbubble] --> optionally the bubbles that contains links
 * 		<#optionBox> --> the translucent and shadowed box that contains the option bubbles
 *  		{.optionbubbles} --> the set of clickeable options the user can choose from 
 *  	</#optionBox>
 *  	<messagesEnd> --> "dummy" div only added to allow the functionality to scroll to bottom
 *  </#chat>
 */
class Chat extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			chatHistory: [],										//array that holds all the messages sent in this session (excluding options and links)
			currentDialog: sliceGraph(graph, vertices[0].getKey()), //array that holds the currently selecting dialog (messages sent by the bot since the last response and options)
			currentlyRenderingMessages: [],							//array that holds the vertices whose types are either bot, user and links that currently should be rendered
			currentlyRenderingOptions: [],							//array that holds the vertices whose types are option that currently should be rendered
			selectedOption: null,									//holds the vertex of the most recent option the user has choosen

			chatVisible: true,										//boolean that decides if the chat should be hidden or shown
			chatFinished: false										//boolean that decides if the chat has already been finished
		};
	}


	componentDidMount() {
		this.setState({
			chatHistory: this.state.currentDialog.filter(message => message.type !== "option"),
		})
		
		this.updateCurrentlyRendering();
	}
	
	componentDidUpdate() {
		if (this.state.chatVisible && !this.state.chatFinished)
			this.scrollToBottom();
		
		document.onclick = (e) => {
			if (this.chatwindow && !this.chatwindow.contains(e.target) && e.target.className !== "option" && e.target.className !== "toggleVisibility" && e.target.id !== "startChat") {
				this.setState(prevState => {
					return {
						chatVisible: false
					}
				})
			}
		}
	}

	/**
	 * Method invoked whenever the user clicks an option.
	 * It calculates the next dialog according to the option clicked and updates the state with the corresponding values
	 * @param {String} optionKey 
	 */
	handleClick(optionKey) {
		//Get the vertex in the Graph corresponding to the key clicked and clone it
		var selectedOption = graph.getVertexByKey(optionKey);
		selectedOption = new GraphVertex(selectedOption.type, selectedOption.value);

		//Obtains the new dialog in Graph starting with the next one after the selected option
		var newDialog = sliceGraph(graph, optionKey).slice(1);
		
		//Updates the state by changing the selected option from "option" to "user" 
		//and pushing it to the history and the messages to render,
		//as well as updating the new dialog as current
		//Invokes updateCurrentlyRendering at the end
		this.setState(prevState => {
			let history = prevState.chatHistory;
			let messages = prevState.currentlyRenderingMessages;
			
			selectedOption.type = "user";
			
			history.push(selectedOption);
			newDialog.filter(message => message.type !== "option").forEach(m => history.push(m));

			messages.push(selectedOption);

			return {
				selectedOption: selectedOption,
				currentDialog: newDialog,
				chatHistory: history,
				currentlyRenderingMessages: messages,
			}
		})
		this.updateCurrentlyRendering();
	}

	/**
	 * This method updates the currently rendering messages and options in the state.
	 * It takes the current dialog in state and push the messages and options
	 * into the corresponding array
	 * In case there are no options left it means the chat is over, so the exit node is pushed instead
	 */
	updateCurrentlyRendering() {
		this.setState(prevState => {
			var messages = prevState.currentlyRenderingMessages;
			var currentDialog = prevState.currentDialog; 
			var options = [];

			currentDialog.forEach(vertex => {
				if (vertex.type !== "option")
					messages.push(vertex);
				else
					options.push(vertex);
			})

			if (options.length === 0)
				options.push(exit);

			return {
				currentlyRenderingMessages: messages,
				currentlyRenderingOptions: options,
			};
		});
	}

	/**
	 * Auxiliar method to always see the last message
	 */
	scrollToBottom() {
		this.messagesEnd.scrollIntoView({ behavior: "smooth" });
	}

	toggleVisibility() {
		this.setState(prevState => {
			return {
				chatVisible: !prevState.chatVisible
			}
		});
	}

	/**
	 * Takes the currently rendering messages and options arrays from the state
	 * and creates chat, link or option bubbles depending on the container array or vertex type
	 * with the data stored within the vertices
	 * 
	 * Returns an array of JSX components with the following structure:
	 * 	{.chatbubbles} --> the set of bubbles sent by the bot and the user
 	 * 	{.linkbubble} --> optionally the bubbles that contains links
 	 * 	<#optionBox> --> the translucent and shadowed box that contains the option bubbles
 	 *  	{.optionbubbles} --> the set of clickeable options the user can choose from 
 	 *  </#optionBox>
	 */
	renderCurrentlyRendering() {
		const messages = this.state.currentlyRenderingMessages;
		const options = this.state.currentlyRenderingOptions;

		const messagesToRender = messages.map(message => {
			return (
				message.type === "link" ? 
				<LinkBubble 
					key = {weakKey({})} 
					k = {message}
				>
					{message.value}
				</LinkBubble> : 
				<ChatBubble 
					key = {weakKey({})} 
					k = {message} 
					sender = {message.type}
				>
					{message.value}
				</ChatBubble>
			)}
		);

		const optionsToRender = options.map(option => {
			return (
				option === exit ? 
				<OptionBubble 
					key = {weakKey({})} 
					k = {option} 
					onClick = {() => {this.setState({chatFinished: true})}}
				>
					{option.value}
				</OptionBubble> : 
				<OptionBubble 
					key = {weakKey({})} 
					k = {option} 
					onClick = {key => this.handleClick(key)}
				>
					{option.value}
				</OptionBubble>
			)}
		);
	
		return messagesToRender.concat(<div key = {weakKey({})} id = "optionsBox">{optionsToRender}</div>);
	}

	render() {
		return (
			this.state.chatFinished ?
				<StartButton/> :
			this.state.chatVisible ?
				<div>
					<img 
						className = "toggleVisibility"
						src = {hideIcon}
						style = {{top: "100px", right: "720px"}}
						onClick = {() => this.toggleVisibility()}
						alt = ""
						/>
					<div 
						id = "chat"
						ref = {(ref) => {this.chatwindow = ref}}
						>
						{this.renderCurrentlyRendering()}
						<div style={{ clear: "both", height: "100px"}} ref={el => { this.messagesEnd = el; }}/>
					</div>
				</div> :

				<img
					className = "toggleVisibility"
					src = {showIcon}
					style = {{top: "100px", right: "0"}}
					onClick = {() => this.toggleVisibility()}
					alt = ""
				/>
		);
	}
}


ReactDOM.render(<StartButton />, document.getElementById("root"));

// ReactDOM.render(<StartButton />, document.getElementById("chatbot"));