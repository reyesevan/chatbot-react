# Tutorial para chatbot

Este tutorial será dividido en tres secciones: cómo correr la aplicación, cómo modificarla para crear nuevos y completamente diferentes chatbots sin necesidad de invertir mucho tiempo y cómo integrarlos a una página web.

**Índice**   
1. [Ejecutando la aplicación para desarrollo](#id0)
2. [Modificación de la aplicación](#id1)
3. [Integración de la aplicación con HTML](#id2)


## Ejecutando la aplicación para desarrollo<a name="id0"></a>

Esta aplicación está programada en React, una librería para JavaScript que permite crear aplicaciones e interfaces complejas y dinámicas a partir de componentes pequeños y reutilizables que se actualizan constantemente con base en un estado interno.

El código se encuentra completamente comentado y documentado, así que entender lo que está pasando no debería ser tan difícil; de cualquier modo, ha sido programado de tal manera que no se requiere que se adentren o conozcan la lógica detrás del chatbot para crear nuevos.

Para correr la aplicación es necesario tener instalado [Node.js][node], puedes hacer un rápido chequeo ejecutando el siguiente comando en cualquier terminal (el signo $ únicamente indica que debe ser ejecutado en una terminal):

    $ npm

Si se reconoce el comando, entonces no es necesario instalar nada, de otro modo, es necesario instalarlo.

Una vez Node esté instalado, la manera más fácil de comenzar es ejecutar el siguiente comando en cualquier carpeta que se desee trabajar:

    $ npx create-react-app chatbot

Tardará unos minutos en crear la aplicación. Cuando finalice, entre a la carpeta `chatbot` y se sugiere eliminar la carpeta `src/` creada dentro del proyecto. A continuación, clone este repositorio y mueva todos los elementos a la carpeta creada por npx. Todo esto puede realizarse simplemente ejecutando los siguientes comandos:

    $ cd chatbot
    $ rm -r src/
    $ git clone https://gitlab.com/reyesevan/chatbot-react.git
    $ cd chatbot-react
    $ mv * ..

A este punto la aplicación ya debería poder funcionar sin ningún problema. Bastará regresar a la carpeta chatbot y ejecutar el siguiente comando:

    $ npm start

Tomará algunos momentos y debería mostrar un output como el siguiente: 

    Compiled successfully!

    You can now view chatbot in the browser.

    Local:            http://localhost:3000/
    On Your Network:  http://192.168.1.78:3000/

    Note that the development build is not optimized.
    To create a production build, use npm run build.

En ese momento el navegador por defecto debería haberse abierto y a partir de este momento se puede probar la aplicación directamente en la dirección y puerto indicados.

Alternativamente, se puede visualizar, editar y probar la aplicación online en [Codesandbox][codesandbox]. No es necesario instalar nada, excepto por quizás crear una cuenta para hacer *fork* del proyecto.

## Modificación de la aplicación<a name="id1"></a>

Todo el código de la aplicación se encuentra dentro del archivo `src/index.js`, mientras que todo el estilo se encuentra dentro de `src/style.css`, pero no es necesario conocerlos a fondo para poder modificarlos.

Lo único que es necesario saber es que es utilizado un grafo dirigido para modelar la información que el bot brindará al usuario, las opciones que éste puede elegir en cada momento y lo que el bot responderá a cada una de ellas.

**Nota:** *si no sabe qué es un grafo, acceda a este [link][directedGraph]*

Por lo tanto, para crear nuevos diálogos para un chat completamente nuevo, bastará con editar o sustituir el grafo dentro del código con los nuevos diálogos, **no es necesario rehacer nada lógico ni crear nuevos y confusos elementos en HTML.**  
**Se recomienda ampliamente realizar primero una guía visual de su nuevo grafo** de diálogos para que sea más sencillo de escribir después en código. A continuación, se muestra la representación gráfica del grafo de diálogos utilizado para el chatbot de Redmind.

![Redmind-directed-graph][redmindGraph]
En esta imagen, cada caja de texto corresponde a un **vértice** (*vertex*) y cada línea corresponde a un **arista** (edge). Todos los aristas están dirigidos hacia abajo a menos que se explicite lo contrario.

Los números mostrados a un lado de cada vértice corresponden al índice del arreglo contenedor de vértices dentro del código, como se ilustra a continuación:

    const vertices = [
        new GraphVertex("bot", "Bienvenido"),                           //0
        new GraphVertex("bot", "Mi nombre es B-Bot"),                   //1
        new GraphVertex("bot", "¿Te puedo ayudar?"),                    //2
        new GraphVertex("option", "Sí, por favor"),                     //3
        new GraphVertex("option", "No, gracias"),                       //4
        new GraphVertex("bot", "¿En qué servicio estás interesado?"),   //5
        new GraphVertex("option", "Desarrollo web y Hosting"),          //6
        new GraphVertex("option", "Producción visual"),                 //7
        new GraphVertex("option", "Social Media"),                      //8
        new GraphVertex("option", "Branding"),                          //9
    ...

Como es posible observar, cada nodo en el mapa mostrado gráficamente corresponde con un nuevo objeto de clase `GraphVertex(type, value)`, donde `value` es el texto que el usuario verá, mientras que `type` debe corresponder a alguno de los siguientes tipos:
- `"bot"`: use este tipo si el vértice será "dicho" por el bot
- `"option"`: use este tipo si el vértice debe ser una de las opciones que el usuario puede elegir
- `"link"`: use este tipo si el vértice será un enlace

Una vez que se tengan todos los vértices del nuevo chat, es posible proceder a conectarlos; para ello, estos vértices creados serán conectados mediante aristas (en inglés ***edges***). A continuación, se muestran los primeros aristas que conectan los vértices declarados en la anterior caja de código.

    const edges = [
        new GraphEdge(vertices[0], vertices[1]),
        new GraphEdge(vertices[1], vertices[2]),
        new GraphEdge(vertices[2], vertices[3]),
        new GraphEdge(vertices[2], vertices[4]),
        new GraphEdge(vertices[3], vertices[5]),
        new GraphEdge(vertices[5], vertices[6]),
        new GraphEdge(vertices[5], vertices[7]),
        new GraphEdge(vertices[5], vertices[8]),
        new GraphEdge(vertices[5], vertices[9]),
        ...

Cada arista es representado por un nuevo objeto de clase `GraphEdge(startVertex, endVertex)`, donde ambos parámetros deben de ser de tipo `GraphVertex()`, el primero representa el vértice de salida y el segundo el vértice de entrada. En este pequeño fragmento de código se conecta el vértice de índice 0 con el vértice de índice 1, es decir, se le está indicando al chatbot que después de mostrar el vértice 0 (*"Bienvenido"*) se proseguirá al vértice 1 (*"Mi nombre es B-Bot"*), y así sucesivamente. Cuando se deseen conectar las opciones se parte de un solo vértice (por ejemplo el vértice 2) con los vértices 3 y 4 que dan las opciones de *"Sí, por favor"* y *"No, gracias"*, respectivamente.

Se realiza lo mismo hasta tener todo el diálogo modelado dentro de ambos arreglos. 

Finalmente, se puede personalizar el nodo con el que el usuario cerrará la conversación cuando ésta finalice modificando el mensaje del siguiente vértice:  
    
    const exit = new GraphVertex("option", "Haga clic aquí para salir");

Otras modificaciones que se pueden realizar fácilmente es cambiar los íconos en general de la aplicación simplemente modificando los enlaces de las siguientes variables:

    const botavatar = "http://www.redmind.mx/demo/berna-icon.png";
    const useravatar = "https://cdn3.iconfinder.com/data/icons/business-avatar-1/512/7_avatar-512.png";

    const link = "http://www.redmind.mx/contacto.html";
    const linkIcon = "http://icon-park.com/imagefiles/link_icon_red.png";

    const hideIcon = "https://image.flaticon.com/icons/png/512/106/106902.png";
    const showIcon = "https://cdn3.iconfinder.com/data/icons/office-general-4/64/eye-512.png";

## Integración de la aplicación con una página web HTML<a name="id2"></a>

Se presentan dos maneras de integrar la aplicación con una página web ya existente: 

### Mucho código agregado a la página web con pocos pasos intermedios

Una alternativa (más recomendada) es utilizar las herramientas mismas de `npm`, esto requiere únicamente ejecutar un comando y mover algunos archivos, pero ocupará más espacio en el HTML de la página. Una vez que se finalice el chatbot y esté listo para ser integrado, siga los siguientes pasos: 

1. Dentro de la carpeta del proyecto de React ejecute el siguiente comando:

        $ npm run build

    Debería arrojar el siguiente output:

        > chatbot@0.1.0 build /run/media/reyes/HDD/R/HTML5/React/chatbot
        > react-scripts build

        Creating an optimized production build...
        Compiled successfully.

        File sizes after gzip:

        36.83 KB  build/static/js/2.d6a534e5.chunk.js
        4.18 KB   build/static/js/main.a16b73e1.chunk.js
        1.17 KB   build/static/css/main.4c1edac1.chunk.css
        762 B     build/static/js/runtime~main.a8a9905a.js

    Esto generará una serie de *chunks* dentro de la carpeta `build/static/` que contendrán el código necesario para ser integrado en una página web, de hecho, se crea una página web de demostración en el archivo `build/index.html`. El código dentro de este archivo html será utilizado para integrar la aplicación a la página web deseada.

2. Copie la carpeta `build` generada dentro de la carpeta de la página web donde se integrará el chatbot
3. Abra el archivo `build/index.html`
4. Copie del archivo todo el código dentro de las etiquetas `<body/>` y la última línea dentro de `<head/>` (que contiene la acoplación de la hoja de estilo)
5. Pegue estas líneas de código en cualquier parte de la página web

****Nota:*** Procure asegurarse de que las rutas relativas a los archivos de los scripts agregados sean válidos con respecto al nuevo archivo.*

### Poco código agregado con compilación manual

Esta solución requiere descargar una nueva herramienta y utilizar un transpilador en línea (o descargar y configurar otra herramienta más), pero tiene la ventaja de que sólo se necesitan agregar 5 líneas al final del HTML y son muy fáciles de controlar. Una vez que se finalice el chatbot y esté listo para ser integrado, siga los sigiuentes pasos:

1. Copie todo el contenido de la carpeta `src/` del proyecto (incluyendo las carpetas) en la carpeta donde se encuentra el `index.html` de su página web
2. Agregue en su página web un nuevo `div` con un ID legible (ahí se colocará el chatbot)
        
        <div id="chatbot"></div>

3. Añada un nuevo `link` para acoplar la hoja de estilo

        <link rel="stylesheet" href="style.css">

4. Acceda a este [enlace][babel]
5. Copie y pegue todo el código de su chatbot en `index.js` en el editor de lado izquierdo de la página
   1. Dentro del editor izquierdo de Babel, elimine las 3 primeras líneas de código (donde se importa React y la hoja de estilo)
   2. Modifique la última línea para que `<StartButton/>` se renderice en el elemento de ID que coincida con el del `div` declarado en el primer paso

            ReactDOM.render(<StartButton />, document.getElementById("chatbot"));

    1. Una vez que Babel realice la transpilación, copie y pegue todo el código generado de lado derecho de vuelta en su archivo `index.js` sustituyendo todo el código anterior

        **Nota importante:** *todo esta sustitución de código se realiza en el `index.js` ya copiado a la carpeta de la página web, no directamente en la carpeta del proyecto de React.* 

6. Instale [Webpack][webpack] ejecutando los siguientes comandos en cualquier terminal:

        $ npm init -y
        $ npm install webpack --save-dev
        $ npm install webpack-cli --save-dev

7. Coloque su terminal en la carpeta de su página web y ejecute el siguiente comando para generar un paquete de un sólo script que contenga todo el código y sus dependencias:

        $ webpack index.js

    Tomará de algunos segundos a un minuto dependiendo de la potencia de su máquina y debería generar una salida como ésta:

        Hash: 7f8fa01aa0539283a131
        Version: webpack 4.35.3
        Time: 1102ms
        Built at: 07/20/2019 2:58:16 PM
        Asset      Size  Chunks             Chunk Names
        main.js  16.5 KiB       0  [emitted]  main
        Entrypoint main = main.js
        [0] ./index.js 27.7 KiB {0} [built]
        [1] ./graph/Graph.js 4.44 KiB {0} [built]
        [2] ./graph/GraphEdge.js 761 bytes {0} [built]
        [3] ./graph/GraphVertex.js + 3 modules 9.4 KiB {0} [built]
            | ./graph/GraphVertex.js 2.59 KiB [built]
            | ./linked-list/LinkedList.js 4.92 KiB [built]
            | ./linked-list/LinkedListNode.js 216 bytes [built]
            | ./utils/comparator/Comparator.js 1.69 KiB [built]

    Una vez termine, se habrá generado un nuevo archivo en una nueva carpeta: `dist/main.js`, **éste será el script que se integrará a la página web.**

8. Agregue las siguientes líneas al HTML de la página web y esto será todo:

        <script src="https://unpkg.com/react@16/umd/react.development.js" crossorigin></script>
        <script src="https://unpkg.com/react-dom@16/umd/react-dom.development.js" crossorigin></script>
        <script src="dist/main.js" type="module" type = "text/babel"></script>
    
    Los dos primeros scripts cargan React y el tercero será el chatbot en sí mismo.

[directedGraph]: http://163.10.22.82/OAS/estructuras_de_grafos/grafo_dirigido.html
[redmindGraph]: /resources/graph.png "Grafo del chatbot de Redmind numerado con respecto al código"
[node]: https://nodejs.org/en/download/
[codesandbox]: https://codesandbox.io/s/gdxes
[babel]: https://babeljs.io/repl
[webpack]: https://webpack.js.org/